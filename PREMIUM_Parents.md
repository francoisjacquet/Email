Email Parents module
====================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot.png)

FEATURES
--------

4 programs will let you send Report Cards, Days Absent, Discipline Logs and Student Balances to parents, directly from within RosarioSIS.

Notifications:

- Teachers can notify parents, directly from the Take Attendance program, when parents are late or absent
- Automatically send (child’s) Birthday notifications
- Automatically send Payment reminders (outstanding fees), X days before or after Due date

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot2-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot2.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot3-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot3.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot4-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot4.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot5-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot5.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot6-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot6.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot7-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot7.png)

### [➭ Get this module](https://www.rosariosis.org/modules/email-parents/)
