Email Students module
=====================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot.png)

FEATURES
--------

3 programs will let you send Report Cards, Discipline Logs and Student Balances to students, directly from within RosarioSIS.

Notifications:

- Automatically send Absence notifications, after X registered absences
- Automatically send Birthday notifications
- Automatically send Payment reminders (outstanding fees), X days before or after Due date

Additionally, teachers can notify students, directly from the Assignments program, when they add a new assignment.

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot-300x193.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot2-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot2.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot3-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot3.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot4-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot4.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot5-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot5.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot6-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot6.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot7-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot7.png)

### [➭ Get this module](https://www.rosariosis.org/modules/email-students/)
