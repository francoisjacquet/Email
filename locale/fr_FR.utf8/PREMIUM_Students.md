Module Email aux élèves
=======================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot-fr.png)

CARACTÉRISTIQUES
----------------

3 programmes vous permettront d’envoyer les Bulletins de notes, les Journaux de discipline et les Soldes des élèves aux élèves, directement depuis RosarioSIS.

Notifications :

- Envoyer automatiquement des notifications d’absence, après X absences enregistrées
- Envoyer automatiquement des notifications d’anniversaire
- Envoyer automatiquement des relances de paiement (frais impayés), X jours avant ou après la date d’échéance

Additionnellement, l’envoi de notifications aux élèves, depuis le programme Devoirs, sera possible pour les enseignants lorsqu’un devoir est créé.

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot-fr-300x193.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot-fr.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot2-fr-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot2-fr.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot3-fr-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot3-fr.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot4-fr-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot4-fr.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot5-fr-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot5-fr.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot6-fr-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot6-fr.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot7-fr-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot7-fr.png)

### [➭ Obtenir ce module](https://www.rosariosis.org/fr/modules/email-students/)
