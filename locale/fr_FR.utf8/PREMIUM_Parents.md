Module Email aux parents
========================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot-fr.png)

CARACTERÍSTICAS
---------------

4 programmes vous permettront d’envoyer les bulletins de notes, les jours d’absence, les journaux de discipline et les soldes des élèves aux parents, directement depuis RosarioSIS.

Notifications :

- L’envoi de notifications aux parents, depuis le programme Saisir les absences, sera possible pour les enseignants lorsque les élèves sont absents ou arrivent en retard.
- Envoyer automatiquement des notifications d’anniversaire (des enfants)
- Envoyer automatiquement des relances de paiement (frais impayés), X jours avant ou après la date d’échéance

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot-fr-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot-fr.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot2-fr-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot2-fr.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot3-fr-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot3-fr.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot4-fr-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot4-fr.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot5-fr-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot5-fr.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot6-fr-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot6-fr.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot7-fr-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot7-fr.png)

### [➭ Obtenir ce module](https://www.rosariosis.org/fr/modules/email-parents/)
