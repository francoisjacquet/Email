Módulo Email a los Estudiantes
==============================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot-es.png)

CARACTERÍSTICAS
---------------

3 programas le permitirán enviar los Boletines de Calificaciones, los Historiales Disciplinarios y los Saldos de los Estudiantes a los estudiantes, directamente desde RosarioSIS.

Notificaciones:

- Enviar notificaciones de Ausencia automáticamente, después de X ausencias registradas
- Enviar notificaciones de Cumpleaños automáticamente
- Enviar Recordatorios de Pago (cobros pendientes), X días antes o después de la fecha de plazo, automáticamente

Adicionalmente, los docentes podrán notificar los estudiantes, directamente desde el programa Tareas, cuando asignan una tarea.

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot-es-300x193.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot-es.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot2-es-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot2-es.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot3-es-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot3-es.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot4-es-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot4-es.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot5-es-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot5-es.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot6-es-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot6-es.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot7-es-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/02/module-email-students-screenshot7-es.png)

### [➭ Obtener este módulo](https://www.rosariosis.org/es/modules/email-students/)
