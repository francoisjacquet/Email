Módulo Email a los Padres
=========================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot-es.png)

CARACTERÍSTICAS
---------------

4 programas le permitirán enviar los Boletines de Calificaciones, los Días de Ausencia, los Historiales Disciplinarios y los Saldos de los Estudiantes a los padres, directamente desde RosarioSIS.

Notificaciones:

- Los docentes pueden notificar los padres, directamente desde el programa Tomar Asistencia, cuando los estudiantes registran una ausencia o tardanza
- Enviar notificaciones de Cumpleaños (del hijo) automáticamente
- Enviar Recordatorios de Pago (cobros pendientes), X días antes o después de la fecha de plazo, automáticamente

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot-es-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot-es.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot2-es-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot2-es.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot3-es-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot3-es.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot4-es-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot4-es.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot5-es-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot5-es.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot6-es-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot6-es.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot7-es-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/11/module-email-parents-screenshot7-es.png)

### [➭ Obtener este módulo](https://www.rosariosis.org/es/modules/email-parents/)
