��    
      l      �       �      �      �        
   (     3     S     p     }     �  �  �     z     �  !   �     �  !   �  %   �          $  	   ,                                      	   
    Already sent Files Attached Select Student email field Send Email Send Email to Selected Students Send Email to Selected Users Send copy to Subject [Copy] Project-Id-Version: Email module
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 17:41+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Že poslano Pripete datotetke Izberite polje za e-pošto dijaka Pošlji sporočilo Pošlji e-pošto izbranim dijakom Pošlji e-pošto izbranim uporabnikom Pošlji kopijo Predmet [Kopiraj] 