Email module
============

![screenshot](https://gitlab.com/francoisjacquet/Email/raw/master/screenshot.png?inline=false)

https://www.rosariosis.org/modules/email/

Version 11.0 - January, 2025

License GNU/GPLv2 or later

Author François Jacquet

Sponsored by Microtec Guatemala & English National Program

DESCRIPTION
-----------
This additional module extends the following modules:

- Students: Send Email
- Users: Send Email

The email body is customizable.

Attach multiple files (up to 24MB).

(Students only) Send a copy to administrators and teachers.

A test email can be sent before sending to students or users.

Translated in [French](https://www.rosariosis.org/fr/modules/email/), [Spanish](https://www.rosariosis.org/es/modules/email/), Slovenian and Portuguese (Brazil).

Note: You can save and manage email templates with the help of the [Templates plugin](https://wwww.rosariosis.org/plugins/templates/).

CONTENT
-------
Students
- Send Email

Users
- Send Email

INSTALL
-------
Copy the `Email/` folder (if named `Email-master`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School > Configuration > Modules_ and click "Activate".

Requires RosarioSIS 5.2+
